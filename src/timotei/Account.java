/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package timotei;

/**
 *
 * @author n1602
 */
public class Account {
    String username;
    String password;
    
    public Account(String un, String pw) {
        username = un;
        password = pw;
    }
    
    public boolean checkLoginInformation(String un, String pw) {
        boolean success = false;
        if ((username == null ? un == null : username.equals(un)) && (password == null ? pw == null : password.equals(pw))) {
            success = true;
        }
        return success;
    }
    
    public String getUsername() {
        return username;
    }
}
