/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package timotei;

import java.util.ArrayList;

/**
 *
 * @author n1602
 */
public class AccountManager {
    private static AccountManager manager = null;
    
    private final ArrayList<Account> accounts; 
    
    private AccountManager() {
        accounts = new ArrayList();
        
        accounts.add(new Account("asd", "s"));
        
    }
    
    public static AccountManager getInstance() {
        if (manager == null) {
            manager = new AccountManager();
        }
        return manager;
    }

    public boolean login(String username, String password) {
        boolean loginSuccess = false;
        
        for (Account a : accounts) {
            if (a.checkLoginInformation(username, password)) {
                loginSuccess = true;
                break;
            }
        }
        
        return loginSuccess;
    }
    
    

    public boolean register(String username, String password1, String password2) {
        boolean registerSuccess = false;
        
        
        if (password1.equals(password2)) {
            
            boolean accountIsUnique = true;
            for (Account a : accounts) {
                if (username.equals(a.getUsername())) {
                    accountIsUnique = false;
                    break;
                }
            }
            
            if (accountIsUnique) {
                accounts.add(new Account(username, password1));
                registerSuccess = true;
            }
        }
        
        return registerSuccess;
    }
    
}
