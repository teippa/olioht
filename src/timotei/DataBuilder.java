/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package timotei;


import java.util.ArrayList;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Stores smartpost data from http://smartpost.ee/fi_apt.xml to ArrayList 
 * locations, from where it can be used.
 * @author n1602
 */
public class DataBuilder {
    //private String locations_URL = "http://smartpost.ee/fi_apt.xml";
    private final String locations_URL = "http://iseteenindus.smartpost.ee/api/?request=destinations&country=FI&type=APT"; // Pakettiautomaatteja
    private final String locations_XML = "locations.xml";
    //private String locations_URL_2 = "http://iseteenindus.smartpost.ee/api/?request=destinations&country=FI&type=PO"; // Postin toimipisteitä
    private final Document doc_locations;
    
    ArrayList<Location> locations;
    
    WebParser parser;
    
    public DataBuilder() {
        
        parser = new WebParser();
        locations = new ArrayList();
        
        String sourceData = null;
        sourceData = parser.readSource(locations_URL);
        
        
        if (sourceData == null) {
            doc_locations = parser.parseXMLFile(locations_XML);
        } else {
            doc_locations = parser.parseXMLString(sourceData);
        }
        
        if (doc_locations != null) {
            getLocationData();
        }
    }
    
    
    
    
    private void getLocationData() {
        NodeList nodes = doc_locations.getElementsByTagName("item");
        
        if (nodes == null) {
            System.out.println("Nodes not found");
            return;
        }
        
        for(int i = 1; i < nodes.getLength(); i++) {
            Node node = nodes.item(i);
            
            if (node.getNodeType() == Node.ELEMENT_NODE) {

                Element eElement = (Element) node;
                
                String newCode = parser.getValue("postalcode", eElement);
                String newCity = parser.getValue("city", eElement).toUpperCase();
                String newAddress = parser.getValue("address", eElement);
                String newAvailabilityStr = parser.getValue("availability", eElement);
                String newPostoffice = parser.getValue("name", eElement);
                String newLat = parser.getValue("lat", eElement);
                String newLng = parser.getValue("lng", eElement);
                
                
                locations.add(new Location(newCode, newCity, newAddress, newAvailabilityStr, newPostoffice, newLat, newLng));
                
            } else {
                System.out.println("Node not found.");
            }
            
        }
    }
    
    public ArrayList getLocations() {
        return locations;
    }


    public ArrayList getLocationsInCity(String cityName) {
        ArrayList<Location> places = new ArrayList();
        
        for (Location l : locations) {
            if (cityName.equalsIgnoreCase(l.getCity())) {
                places.add(l);
            }
        }
        return places;
    }
    


}


