/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package timotei;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author n1602
 */
public class DataIE {
    
    Storage storage;
    
    String itemDataFile;
    String packageDataFile;
    String lokiFile;
    
    public DataIE(Storage s) {
        itemDataFile = "itemData.txt";
        packageDataFile = "packageData.txt";
        lokiFile = "loki.txt";
        
        storage = s;
    }
    
    
    /**
     * Reads item data from itemData.txt to ArrayList items in storage
     */
    public void importItems() {
        BufferedReader in;
        
        //System.out.println("Item import:");
        
        try {
            in = new BufferedReader(new InputStreamReader(new FileInputStream(itemDataFile), StandardCharsets.UTF_8));
            
            String inputLine;
            
            while ((inputLine = in.readLine()) != null) {
                //System.out.println(inputLine);
                
                String[] params = inputLine.split(";");
                
                try {
                    storage.addNewItem(params[0], params[1], params[2], "1".equals(params[3]));
                    //items.add(new Item(params[0], params[1], params[2], "1".equals(params[3]) ));
                } catch (Exception e) {
                    System.out.println("Jonkin tavaran lisääminen epäonnistui:\n\t"+ params[1]);
                }
            }
            
            in.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Storage.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Storage.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * Writes item data from ArrayList items in storage to itemData.txt
     */
    public void exportItems() {
        BufferedWriter out;
        
        ArrayList<Item> items = storage.getItems();
        
        try {
            out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(itemDataFile), StandardCharsets.UTF_8));
            

            for (Item i : items) {

                String line = i.getName() + ";" + i.getSizeStr() + ";" + i.getWeight() + ";";

                if (i.getCanBreak()) {
                    line += "1";
                } else {
                    line += "0";
                }

                out.write(line);
                out.newLine();
            }
            
            out.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Storage.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Storage.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * Reads package data from packageData.txt to ArrayList packages in storage
     */
    public void importPackages() {
        BufferedReader in;
        
        //System.out.println("Package import:");
        
        try {
            in = new BufferedReader(new InputStreamReader(new FileInputStream(packageDataFile), StandardCharsets.UTF_8));
            
            String inputLine;
            
            while ((inputLine = in.readLine()) != null) {
                String[] params = inputLine.split(";");
                
                storage.addNewPackage(params[1], params[2], params[3], "1".equals(params[4]), params[0]);
                
            }
            
            in.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Storage.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Storage.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * Writes package data from ArrayList packages in storage to packageData.txt
     */
    public void exportPackages() {
        BufferedWriter out;
        ArrayList<PackageClass> packages = storage.getPackages();
        
        try {
            out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(packageDataFile), StandardCharsets.UTF_8));
            

            for (PackageClass pc : packages) {

                String line = pc.getName() + ";" + pc.getItem().getName() + ";" + pc.getItem().getSizeStr() + ";" + pc.getItem().getWeight() + ";";

                if (pc.getItem().getCanBreak()) {
                    line += "1";
                } else {
                    line += "0";
                }

                out.write(line);
                out.newLine();
            }
            
            out.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Storage.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Storage.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * Reads loki.txt and returns its contents.
     * @return total
     */
    public String readLoki() {
        BufferedReader in;
        String total = "";
        
        try {
            in = new BufferedReader(new InputStreamReader(new FileInputStream(lokiFile), StandardCharsets.UTF_8));
            
            String inputLine;
            
            while ((inputLine = in.readLine()) != null) {
                //System.out.println(inputLine);
                
                total = total + inputLine + System.lineSeparator();
                
            }
            
            in.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Storage.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Storage.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return total;
    }
    
    /**
     * Appends string to top of loki.txt
     * @param s 
     */
    public void appendLoki(String s) {
        BufferedWriter out;
        
        String total = readLoki();
        
        try {
            out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(lokiFile), StandardCharsets.UTF_8));
            
            out.append(s + System.lineSeparator() + total);
            
            out.close();
        
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Storage.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Storage.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
