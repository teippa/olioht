/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package timotei;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;

/**
 *
 * @author n1602
 */
public class FXMLDocumentController implements Initializable {
    
    MainClass main;
    WebEngine engine;
    
    
    @FXML
    private WebView map;
    @FXML
    private ComboBox<String> city_selection;
    @FXML
    private Button button_addLocations;
    @FXML
    private Button button_addPath;
    @FXML
    private ComboBox<String> startPoint_selection_city;
    @FXML
    private ComboBox<String> endPoint_selection_city;
    @FXML
    private ComboBox<Location> startPoint_selection_location;
    @FXML
    private ComboBox<Location> endPoint_selection_location;
    @FXML
    private ComboBox<PackageClass> package_selection;
    @FXML
    private ToggleGroup packageClass;
    @FXML
    private ComboBox<Item> selection_item;
    @FXML
    private TextField input_name;
    @FXML
    private TextField input_size;
    @FXML
    private TextField input_weight;
    @FXML
    private CheckBox input_canBreak;
    @FXML
    private ListView<PackageClass> listview_packages;
    @FXML
    private Button button_removePackage;
    @FXML
    private ListView<Item> listview_items;
    @FXML
    private Label label_packages;
    @FXML
    private Label label_items;
    @FXML
    private Label info_1lk;
    @FXML
    private Label info_2lk;
    @FXML
    private Label info_3lk;
    @FXML
    private TextArea textArea_loki;
    
    @FXML
    private TabPane tabPane;
    @FXML
    private Color x1;
    @FXML
    private Rectangle whiteBackround;
    @FXML
    private Font x2;
    @FXML
    private Font x21;
    @FXML
    private Font x22;
    @FXML
    private RadioButton radio_1lk;
    @FXML
    private RadioButton radio_2lk;
    @FXML
    private RadioButton radio_3lk;
    @FXML
    private Button button_addPackage;
    @FXML
    private Label label_error;
    
    
    
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        button_addPath.setDisable(true);
        startPoint_selection_location.getItems().clear();
        endPoint_selection_location.getItems().clear();
        
        engine = map.getEngine();
        engine.load(getClass().getResource("index.html").toExternalForm());
        
        main = MainClass.getInstance(engine);
        
        city_selection.getItems().setAll(main.getCities());
        startPoint_selection_city.getItems().setAll(main.getCities());
        endPoint_selection_city.getItems().setAll(main.getCities());
        
        updatePackageSelection();
        updateItemSelection();
        controlButtons();
        textArea_loki.setText(main.getStorage().readLoki());
        
        setPackageDetails();
        
    }
    
    
    /**
     * Adds all smartpost locations from a city to map
     * @param event 
     */
    @FXML
    private void addLocations(ActionEvent event) {
        String cityName = city_selection.getValue();
        
        main.drawLocationsInCity(cityName);
    }

    /**
     * Updates locations ComboBox when starting city is selected
     * @param event 
     */
    @FXML
    private void startCitySelected(ActionEvent event) {
        String cityName = startPoint_selection_city.getValue();
        if (cityName != null) {
            main.setLocations(startPoint_selection_location, cityName);
        }
        controlButtons();
    }

    /**
     * Updates locations ComboBox when destination city is selected
     * @param event 
     */
    @FXML
    private void endCitySelected(ActionEvent event) {
        String cityName = endPoint_selection_city.getValue();
        if (cityName != null) {
            main.setLocations(endPoint_selection_location, cityName);
        }
        controlButtons();
    }
    
    /**
     * Adds starting location to map
     * @param event 
     */
    @FXML
    private void addStartLocation(ActionEvent event) {
        Location location = startPoint_selection_location.getValue();
        if (location != null) {
            main.drawLocation(location);
        }
    }

    /**
     * Adds destination location to map
     * @param event 
     */
    @FXML
    private void addEndLocation(ActionEvent event) {
        Location location = endPoint_selection_location.getValue();
        if (location != null) {
            main.drawLocation(location);
        }
    }
    
    /**
     * Sends selected package and launches package information window
     * @param event 
     */
    @FXML
    private void addPath(ActionEvent event) {
        
        Location endPoint = endPoint_selection_location.getValue();
        Location startPoint = startPoint_selection_location.getValue();
        
        PackageClass pc = package_selection.getValue();
        
        if (startPoint != endPoint && startPoint != null) {
            if (main.sendPackage(startPoint, endPoint, pc)) {
                main.getNotificationManager().newNotification();
                //launchNotification();
                package_selection.setValue(null);
                
                updatePackageSelection();
                textArea_loki.setText(main.getStorage().readLoki());
                
            } else {
                label_error.setText("1.lk:n pakettia ei voi lähettää kauemmas kuin 150km.");
            }
        } else if (startPoint == endPoint) {
            label_error.setText("Lähtö- ja saapumispisteet eivät voi olla samat.");
        }
        
    }

    /**
     * Removes all paths from map and clears ArrayList packagesOnTheWay from storage
     * Clearing ArrayList causes all package info windows to close.
     */ 
    @FXML
    private void removePaths(ActionEvent event) {
        engine.executeScript("document.deletePaths();");
        
        main.getNotificationManager().closeNotifications();
    }

    
    /**
     * Enables and disables buttons
     * Also clears error label
     * @param event 
     */
    @FXML
    private void controlButtons(ActionEvent event) {
        controlButtons();
    }
    private void controlButtons() {
        Location startPoint = startPoint_selection_location.getValue();
        Location endPoint = endPoint_selection_location.getValue();
        PackageClass paketti = package_selection.getValue();
        
        if (startPoint == null || endPoint == null || paketti == null) {
            button_addPath.setDisable(true);
        } else {
            button_addPath.setDisable(false);
        }
        
        
        String city = city_selection.getValue();
        
        if (city != null) {
            button_addLocations.setDisable(false);
        } else {
            button_addLocations.setDisable(true);
        }
        
        label_error.setText("");
    }
    
    
    /***********  Pakettien hallinta -välilehti  ***********************/

    /**
     * Updates package selection ListView and ComboBox
     */
    private void updatePackageSelection() {
        package_selection.getItems().setAll(main.getStorage().getPackages());
        listview_packages.getItems().setAll(main.getStorage().getPackages());
    }
    /**
     * Updates item selection ListView and ComboBox
     */
    private void updateItemSelection() {
        selection_item.getItems().setAll(main.getStorage().getItems());
        listview_items.getItems().setAll(main.getStorage().getItems());
    }
    
    /**
     * Add a new package to storage.
     * @param event 
     */
    @FXML
    private void addPackage(ActionEvent event) {
        Item selectedItem = selection_item.getValue();
        
        // Try to create a new item
        if (selectedItem == null) {
            String name = input_name.getText();
            String size = input_size.getText();
            String weight = input_weight.getText();
            boolean canBreak = input_canBreak.isSelected();
            
            // Try to create new item, if there are enough parameters.
            if (!name.isEmpty() && !size.isEmpty() && !weight.isEmpty()) {
                
                selectedItem = main.addNewItem(name, size, weight, canBreak);
                
                updateItemSelection();
            }
        }
        
        // If item is selected or item creation was successfull
        if (selectedItem != null) {
            RadioButton b = (RadioButton) packageClass.getSelectedToggle();
            
            // Text is a number with transparent fill
            PackageClass newPackage = main.addNewPackage(selectedItem, b.getText());
            
            
            textArea_loki.setText(main.getStorage().readLoki());
            updatePackageSelection();
        }
        
        selection_item.setValue(null);

        input_name.setText("");
        input_size.setText("");
        input_weight.setText("");
    }
    
    /**
     * Removes a selected item or package from ListView 
     * Called from the same button "Poista paketti"/"Poista tavara"
     * @param event 
     */
    @FXML
    private void removeItemOrPackage(ActionEvent event) {
        Item i = listview_items.getSelectionModel().getSelectedItem();
        PackageClass pc = listview_packages.getSelectionModel().getSelectedItem();
        
        if (pc != null) {
            main.removePackage(pc);
            updatePackageSelection();
        }
        if (i != null) {
            main.removeItem(i);
            updateItemSelection();
        }
        
        textArea_loki.setText(main.getStorage().readLoki());
    }
    
    /**
     * Shows packages list in "Hallitse paketteja"
     * @param event 
     */
    @FXML
    private void listPackages(MouseEvent event) {
        listview_packages.toFront();
        label_packages.setTextFill(Color.BLACK);
        label_items.setTextFill(Color.LIGHTGRAY);
        
        button_removePackage.setText("Poista paketti");
        
        
        listview_items.getSelectionModel().clearSelection();
    }

    /**
     * Shows items list in "Hallitse paketteja"
     * @param event 
     */
    @FXML
    private void listItems(MouseEvent event) {
        listview_items.toFront();
        label_items.setTextFill(Color.BLACK);
        label_packages.setTextFill(Color.LIGHTGRAY);
        
        button_removePackage.setText("Poista tavara");
        
        listview_packages.getSelectionModel().clearSelection();
    }
    
    /**
     * Sets package class details to "Hallitse paketteja"-tab
     */
    private void setPackageDetails() {
        PackageClass pc1 = new PostClass_1();
        PackageClass pc2 = new PostClass_2();
        PackageClass pc3 = new PostClass_3();
        
        info_1lk.setText(pc1.getInfo());
        info_2lk.setText(pc2.getInfo());
        info_3lk.setText(pc3.getInfo());
    }
    
}
