/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package timotei;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.animation.AnimationTimer;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author n1602
 */
public class FXML_infoController implements Initializable {

    
    PackageClass pc;
    
    @FXML
    private AnchorPane anchor;
    @FXML
    private Rectangle loadingBar;
    @FXML
    private Rectangle loadingBarBackground;
    @FXML
    private Label packageInformation;
    @FXML
    private Label label_perilla;
    @FXML
    private Label label_matkalla;
    
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        final ArrayList<PackageClass> packagesOnTheWay = MainClass.getInstance().getStorage().getPackagesOnTheWay();
        //System.out.println(packagesOnTheWay.size());
        pc = packagesOnTheWay.get(packagesOnTheWay.size()-1);
        
        /**
         * Luodaan timeri, jolla tutkitaan milloin paketti on saapunut perille.
         * Aluksi duration on aina 50 sekuntia, sillä suurin osa matkoista kestää
         * suunnilleen sen verran. 
         * index.html tiedostossa lasketaan paketin kuljetusaika, mutta tätä 
         * arvoa ei saada suoraan palautettua funktiosta. Aika täytyy siis 
         * tallentaa dokumentin globaaliksi muuttujaksi ja hakea myöhemmin. 
         * Ajan laskemisessa kuluu aikaa 0.5 - 1 sekuntia, jonka jälkeen se 
         * voidaan päivittää tänne.
         */
        
        AnimationTimer timer;
        timer = new AnimationTimer() {
            double startTime = -1;
            double lastFrame = -1;
            
            double duration = 50;
            
            boolean durationChecked = false;
            
            double loadingBarWidth = 0;
            double maxLoadingBarWidth = loadingBarBackground.getWidth();
            Stage stage;
            
            //Window window = ((Node)(anchor)).getScene().getWindow();
            
            @Override
            public final void handle(long now) {
                if (stage == null) {
                    stage = ((Stage) (anchor).getScene().getWindow());
                    
                    startTime = now/Math.pow(10,9);
                    lastFrame = now;
                }
                
                double t = now/Math.pow(10,9) - startTime;
                
                // Päivitetään paketin oikea saapumisaika
                if (t > 1 && !durationChecked) {
                    double newDuration = MainClass.getInstance().getDuration();
                    if (newDuration != -1) {
                        durationChecked = true;
                        duration = newDuration;
                    }
                }
                
                
                // Lopetetaan timer, jos paketti on perillä, ikkuna suljetaan, tai reitin piirto on keskeytetty
                if (t > duration) {
                    stage.toFront();
                    packageArrived();
                    
                    MainClass.getInstance().getStorage().removePackageOnTheWay(pc);
                    this.stop();
                } else if (!stage.isShowing()) {
                    
                    MainClass.getInstance().getStorage().removePackageOnTheWay(pc);
                    this.stop();
                }
                
                if ( (loadingBarWidth = (t/duration)*maxLoadingBarWidth) > maxLoadingBarWidth ) {
                    loadingBarWidth = maxLoadingBarWidth;
                }
                loadingBar.setWidth(loadingBarWidth);

                lastFrame = now;
            }
        };
        timer.start();
    }
    
    /**
     * Runs when timer finishes
     * Calculates if the package has survived the delivery
     */
    public void packageArrived() {
        label_matkalla.setVisible(false);
        label_perilla.setVisible(true);
        
        boolean isBroken = pc.breakOnDelivery(pc.getItem());
        
        if (isBroken) {
            packageInformation.setText("Paketti saapui perille, mutta ei valitettavasti pysynyt ehjänä.");
        } else {
            packageInformation.setText("Paketti saapui ehjänä perille. Helvetin heino juttu äijjät!");
        }
    }
    
}
