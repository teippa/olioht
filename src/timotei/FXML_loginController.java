/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package timotei;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

/**
 * FXML Controller class for login screen
 *
 * @author n1602
 */
public class FXML_loginController implements Initializable {
    
    private AccountManager manager;

    @FXML
    private PasswordField field_password;
    @FXML
    private TextField field_username;
    @FXML
    private AnchorPane login_pane;
    @FXML
    private Label errorText;
    @FXML
    private Button button_createAccount;
    @FXML
    private Color x1;
    @FXML
    private Color x4;
    @FXML
    private Font x2;
    @FXML
    private Color x3;
    @FXML
    private Button button_login;

    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        manager = AccountManager.getInstance();
        
    }    
    

    @FXML
    private void login(ActionEvent event) {
        String username = field_username.getText();
        String password = field_password.getText();
        
        //enterTIMO();
        
        if (!username.isEmpty() && !password.isEmpty()) {
            
            if (manager.login(username, password)) {
                enterTIMO();
            } else {
                errorText.setText("Väärä käyttäjätunnus tai salasana");
            }
            
        } else {
            errorText.setText("Syötä käyttäjätunnus ja salasana.");
        }
        //*/
    }

    public void enterTIMO() {
        try {

            Parent root = FXMLLoader.load(getClass().getResource("FXMLDocument.fxml"));

            final Stage stage = new Stage();
            stage.setTitle("TIMOTEI");
            stage.setScene(new Scene(root));
            

            stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
                @Override
                public void handle(WindowEvent t) {
                    try {
                        MainClass.getInstance().getStorage().exportItems();
                        MainClass.getInstance().getStorage().exportPackages();
                        MainClass.getInstance().getNotificationManager().closeNotifications();
                    } catch (Exception ex) {
                    }
                    System.out.println("Shutting down.");
                }

            });

            
            ChangeListener widthListener = new ChangeListener(){
                @Override
                public void changed(ObservableValue observableValue, Object oldValue, Object newValue) {
                    //System.out.println(newValue.toString());
                    if(Double.parseDouble(newValue.toString())<1080)
                        stage.setWidth(1095);
                }
            };
            ChangeListener heightListener = new ChangeListener(){
                @Override
                public void changed(ObservableValue observableValue, Object oldValue, Object newValue) {
                    //System.out.println(newValue.toString());
                    if(Double.parseDouble(newValue.toString())<720)
                        stage.setHeight(760);
                }
            };
            
            stage.getScene().widthProperty().addListener(widthListener);
            stage.getScene().heightProperty().addListener(heightListener);
            
            stage.show();
            // Hide this current window (if this is what you want)*/
            ((Node)(login_pane)).getScene().getWindow().hide();

        } catch (IOException ex) {
            errorText.setText("An error occurred while trying to launch program.");
            Logger.getLogger(FXML_loginController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void getNewAccountScreen(ActionEvent event) {
        
        try {
            Parent root = FXMLLoader.load(getClass().getResource("FXML_register.fxml"));
            ((Node)(login_pane)).getScene().setRoot(root);
        } catch (IOException ex) {
            Logger.getLogger(FXML_loginController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
}
