/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package timotei;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

/**
 * FXML Controller class for login screen
 *
 * @author n1602
 */
public class FXML_registerController implements Initializable {
    
    AccountManager manager;

    @FXML
    private AnchorPane login_pane;
    @FXML
    private Color x3;
    @FXML
    private Color x1;
    @FXML
    private TextField field_username;
    @FXML
    private PasswordField field_password;
    @FXML
    private PasswordField field_password2;
    @FXML
    private Color x4;
    @FXML
    private Font x2;
    @FXML
    private Label errorText;
    @FXML
    private Button button_createAccount;
    @FXML
    private Button button_back;

    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        manager = AccountManager.getInstance();
        
    }    
    

    @FXML
    private void register(ActionEvent event) {
        
        String username = field_username.getText();
        String password1 = field_password.getText();
        String password2 = field_password2.getText();
        
        if (!username.isEmpty() && !password1.isEmpty() && !password2.isEmpty()) {
            if (password1.equals(password2)) {
                errorText.setText("");
                if (manager.register(username, password1, password2)) {
                    try {
                        Parent root = FXMLLoader.load(getClass().getResource("FXML_login.fxml"));
                        ((Node)(login_pane)).getScene().setRoot(root);
                    } catch (IOException ex) {
                        //Logger.getLogger(FXML_registerController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                } else {
                    errorText.setText("Käyttäjätunnus on jo varattu.");
                }
            } else {
                errorText.setText("Salasanat eivät täsmää.");
            }
        } else {
            errorText.setText("Syötä käyttäjätunnus ja salasanat.");
        }
    }
    
    
    @FXML
    private void getLoginScreen(ActionEvent event) {

        try {
            Parent root = FXMLLoader.load(getClass().getResource("FXML_login.fxml"));
            ((Node)(login_pane)).getScene().setRoot(root);
        } catch (IOException ex) {
            //Logger.getLogger(FXML_registerController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    

}
