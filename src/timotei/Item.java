/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package timotei;


/**
 *
 * @author n1602
 */
public class Item {
    protected boolean canBreak;
    
    protected String name;
    protected String sizeStr;
    protected double[] size; // {0, 0, 0} cm
    protected double weight; //kg
    
    protected boolean creationSuccess;
    
    /*
    public Item() {
        size = new double[] {0, 0, 0};
        
        name = "Sample item";
        canBreak = false;
        weight = 0;
    }
    */
    
    public Item(String name, String size, String weight, boolean canBreak) {
        creationSuccess = true;
        this.size = parseSize(size);
        
        this.name = name;
        this.canBreak = canBreak;
        this.weight = str2Double(weight);
    }
    
    // for weight calculations
    protected final double str2Double(String str) {
        double dbl;
        try {
            dbl = Double.parseDouble(str);
        } catch (NumberFormatException e) {
            System.out.println("Error in Item.str2Double()!");
            dbl = 0;
            creationSuccess = false;
        }
        
        return dbl;
    }
    
    // for converting size string to double array
    protected final double[] parseSize(String str) {
        double[] newSize = {0, 0, 0};
        sizeStr = str;
        
        try {
            int i = 0;
            for (String s : str.split("\\*")) {
                newSize[i] = Double.parseDouble(s);
                i++;
            }
        } catch (NumberFormatException e) {
            System.out.println("Error in Item.parseSize()!");
            sizeStr = "0*0*0";
            creationSuccess = false;
        }
        
        return newSize;
    }
    
    public boolean isCreationSuccess() {
        return creationSuccess;
    }

    public boolean getCanBreak() {
        return canBreak;
    }
    
    public double[] getSize() {
        return size;
    }
    public String getSizeStr() {
        return sizeStr;
    }
    public double getWeight() {
        return weight;
    }

    public String getName() {
        return name;
    }
    
    @Override
    public String toString() {
        return name +":  "+ sizeStr +" cm, "+ weight +" kg";
    }
    
}

