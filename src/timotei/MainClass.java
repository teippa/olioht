/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package timotei;

import java.util.ArrayList;
import javafx.scene.control.ComboBox;
import javafx.scene.web.WebEngine;

/**
 *
 * @author n1602
 */
public class MainClass {
    
    private static MainClass main = null;
    
    private final NotificationManager notificationManager;
    private final WebEngine engine;
    private final DataBuilder locationManager;
    private final Storage storage;
    
    private MainClass(WebEngine engine) {
        this.engine = engine;
        
        notificationManager = new NotificationManager();
        locationManager = new DataBuilder();
        storage = Storage.getInstance();
    }
    
    public static MainClass getInstance(WebEngine engine) {
        if (main == null) {
            main = new MainClass(engine);
        }
        return main;
    }
    
    public static MainClass getInstance() {
        return main;
    }
    
    public Storage getStorage() {
        return storage;
    }
    
    public void newNotification() {
        notificationManager.newNotification();
    }
    
    public NotificationManager getNotificationManager() {
        return notificationManager;
    }
    
    
    /**
     * Set all smartpost locations in a city to ComboBox
     * @param selection
     * @param cityName 
     */
    public void setLocations( ComboBox<Location> selection, String cityName) {
        ArrayList<Location> locations = locationManager.getLocationsInCity(cityName);

        selection.getItems().setAll(locations);
        
        // If there is only one smartpost in city, select it automatically
        if (locations.size() == 1) {
            selection.setValue(locations.get(0));
        }
    }

    /**
     * Returns string ArrayList of all cities 
     * @return 
     */
    public ArrayList getCities() {
        ArrayList<String> cities = new ArrayList();
        String cityStr;
        
        ArrayList<Location> locations = locationManager.getLocations();
        
        for (Location l : locations) {
            cityStr = l.getCity().toUpperCase();
            if (!cities.contains(cityStr)) {
                cities.add(cityStr);
            }
        }
        return cities;
    }

    /**
     * Draws all smartpost locations in a city to map
     * @param cityName 
     */
    public void drawLocationsInCity(String cityName) {
        
        ArrayList<Location> locations = locationManager.getLocationsInCity(cityName);
        
        for (Location l : locations) {
            drawLocation(l);
        }
    }
    /**
     * Draws a single smartpost location to map
     * @param l 
     */
    public void drawLocation(Location l) {
            String[] args = {l.getAddress(), l.getOther(), "orange"};

            String scriptCommand = "document.goToLocation(\""+ args[0] +"\", \""+ args[1] +"\", \""+ args[2] +"\");";
            engine.executeScript(scriptCommand);
    }
    
    /**
     * Sends the package and calls the javascript drawing function
     * @param startPoint
     * @param endPoint
     * @param pc
     * @return drawingStarted
     */
    public boolean sendPackage(Location startPoint, Location endPoint, PackageClass pc) {
        boolean drawingStarted = false;
        int packageClass = Integer.parseInt(pc.getName());
        String color = "red";
        ArrayList<String> coords = new ArrayList();
        double distance;
        
        coords.add(startPoint.getLat());
        coords.add(startPoint.getLng());
        coords.add(endPoint.getLat());
        coords.add(endPoint.getLng());
        
        String scriptCommand = "document.getDistance("+ coords +");";
        try {
            distance = (double)engine.executeScript(scriptCommand);
        } catch (Exception e1) {
            System.out.println("Distance ei ollut double.");
            try {
                distance = (int)engine.executeScript(scriptCommand);
            } catch (Exception e2) {
                distance = 0;
            }
                
        }
        
        
        if (packageClass == 1 && distance > 150) {
            System.out.println("Liian pitkä matka!");
        } else {
            scriptCommand = "document.createPath("+ coords +", \""+ color +"\", "+ packageClass +");";
            engine.executeScript(scriptCommand);
            
            drawingStarted = true;
            storage.updateLoki(pc, "send");
            storage.addPackageOnTheWay(pc);
            storage.removePackage(pc);
            
        }
        return drawingStarted;
    }
    
    /**
     * gets the delivery time of last sent package.
     * @return 
     */
    public double getDuration() {
        double duration = -1;
        String scriptCommand = "document.getDuration();";
        
        try {
            duration = (double) engine.executeScript(scriptCommand);
        } catch (Exception e1) {
            try {
                int d = (int) engine.executeScript(scriptCommand);
                duration = (int) d;
            } catch (Exception e2) {
                System.out.println("Väärää tyyppiä (ei ollut int)");
            }
            System.out.println("Väärää tyyppiä (ei ollut double)");
        }
        
        return duration;
    }

    /**
     * Try to add a new item to storage. 
     * If adding is successful, update log
     * Returns null, if adding failed.
     * @param name
     * @param size
     * @param weight
     * @param canBreak
     * @return 
     */
    public Item addNewItem(String name, String size, String weight, boolean canBreak) {
        Item i = storage.addNewItem(name, size, weight, canBreak);
        if (i != null) {
            storage.updateLoki(i, "add");
        }
        return i;
    }

    /**
     * Try to add a new package to storage. 
     * If adding is successful, update log
     * Returns null, if adding failed.
     * @param selectedItem
     * @param text
     * @return 
     */
    public PackageClass addNewPackage(Item selectedItem, String text) {
        PackageClass pc = storage.addNewPackage(selectedItem, text);
        if (pc != null) {
            storage.updateLoki(pc, "add");
        }
        return pc;
    }

    /**
     * Removes package from storage
     * @param pc 
     */
    public void removePackage(PackageClass pc) {
        if (storage.removePackage(pc)) {
            storage.updateLoki(pc, "remove");
        }
    }

    /**
     * Removes item from storage
     * @param i 
     */
    public void removeItem(Item i) {
        if (storage.removeItem(i)) {
            storage.updateLoki(i, "remove");
        }
    }
    

    
}
