/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package timotei;

import com.sun.glass.ui.Screen;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author n1602
 */
public class NotificationManager {
    
    ArrayList<Stage> notifications;
    
    private final int notificationHeight;
    private final int notificationWidth;
    
    public NotificationManager() {
        
        notifications = new ArrayList();
        
        notificationHeight = 146;
        notificationWidth = 388;
        
    }
    
    
    /**
     * Launches a new notification to upper left side of the screen
     */
    public void newNotification() {
        double x; double y;
        double len;
        
        ArrayList<Stage> deadWindows = new ArrayList();
        for (Stage s : notifications) {
            if (s.isShowing()) {
                deadWindows.clear();
            } else {
                deadWindows.add(s);
            }
        }
        notifications.removeAll(deadWindows);
        
        len = notifications.size();
        int screenHeight = Screen.getMainScreen().getVisibleHeight();
        int screenWidth = Screen.getMainScreen().getVisibleWidth();
        
        y = 10 + len*notificationHeight/2;
        x = 10;
        
        while (y+notificationHeight > screenHeight) {
            if ( (x = x+notificationWidth/2) > (screenWidth - notificationWidth)) {
                x = screenWidth - notificationWidth;
            }
            y = y-(screenHeight-notificationHeight);
        }
        
        notifications.add(launchNotification(x, y));
    }
    
    
    /**
     * Opens a new window with sent package information.
     * @return 
     */
    private Stage launchNotification(double x, double y) {
        
        Stage notificationStage;
        notificationStage = new Stage();
        
        try {
            Parent root1 = FXMLLoader.load(getClass().getResource("FXML_info.fxml"));
            Scene scene = new Scene(root1, notificationWidth, notificationHeight);
            
            notificationStage.setX(x);
            notificationStage.setY(y);
            
            notificationStage.setScene(scene);
            notificationStage.setTitle("Paketti");
            notificationStage.setResizable(false);

            notificationStage.show();
            
        } catch (IOException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
            notificationStage = null;
        }
        return notificationStage;
    }
    
    /**
     * Closes all package windows and clears ArrayList packagesOnTheWay
     */
    public void closeNotifications() {
        for (Stage s : notifications) {
            s.hide();
            s.close();
        }
        notifications.clear();
        MainClass.getInstance().getStorage().getPackagesOnTheWay().clear();
    }
}
