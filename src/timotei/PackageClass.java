/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package timotei;


/**
 *
 * @author n1602
 */
abstract class PackageClass {
    protected double maxDeliveryDistance;
    //protected double deliverySpeed;
    
    protected String name;
    
    protected double[] maxSize;
    protected double maxWeight;
    
    String information;
    
    Item item;
    
    public PackageClass() {
        item = null;
        
        name = "";
        information = "";
        
        maxSize = new double[] {0, 0, 0};
        maxWeight = 0;
        
        maxDeliveryDistance = 0;
        //deliverySpeed = 0;
        
    }
    
    /**
     * Hajoaa jos on hajotakseen (käytetään 1.lk kuljetuksissa muokkaamattomana)
     * @param i
     * @return 
     */
    protected boolean breakOnDelivery(Item i) {
        return i.getCanBreak();
    }
    
    /**
     * Testataan, onko tavara mahdollista kuljettaa kyseisessä luokassa.
     * @param i
     * @return 
     */
    public boolean isCompatible(Item i) {
        boolean isCompatible = true;
        double[] iSize = i.getSize();
        // Testataan paino
        if (i.getWeight() <= maxWeight) {
            
            // Testataan mittasuhteet
            for (int n=0 ; n<3 ; n++) {
                if (iSize[n] <= maxSize[n]) {
                    // tavara mahtuu pakettiin
                } else {
                    isCompatible = false;
                    break;
                }
            }
        } else {
            // liian painava
            isCompatible = false;
        }
        return isCompatible;
    }
    
    // Lisätään tavara pakettiin
    public final boolean addItem(String name, String size, String weight, boolean canBreak) {
        boolean success;
        Item i = new Item(name, size, weight, canBreak);
        
        success = addItem(i);
        
        return success;
    }
    public final boolean addItem(Item i) {
        boolean success;
        if (success = (isCompatible(i) && i.isCreationSuccess())) {
            item = i;
        } else {
            System.out.println("Item not compatible.");
        }
        return success;
    }
    
    // Palauttaa paketin lähetysluokan (1,2,3)
    public String getName() {
        return name;
    }
    
    public Item getItem() {
        return item;
    }
    
    public double[] getMaxSize() {
        return maxSize;
    }
    public double getMaxWeight() {
        return maxWeight;
    }
    
    @Override
    public String toString() {
        return item.getName() + " (" + name + ".lk)";
    }
    public String toLoki() {
        return item.getName() + " (" + name + ".lk)";
    }
    
    public String getInfo() {
        String s = 
                information +" \n"+
                "Maksimikoko:    "+ maxSize[0] +"cm * "+ maxSize[1] +"cm * "+ maxSize[2] +"cm.\n"+
                "Maksimipaino:   "+ maxWeight +"kg.";
        
        return s;        
    }
    
}

/*******************************************************/


class PostClass_1 extends PackageClass {
    
    public PostClass_1() {
        
        name = "1";
        information = "Nopea, mutta särkyvä paketti hajoaa matkalla. (Max 150km.)";
        maxSize = new double[] {100, 100, 100};
        maxWeight = 30;
        maxDeliveryDistance = 150;
        
    }
}

class PostClass_2 extends PackageClass {

    public PostClass_2() {
        
        name = "2";
        information = "Hitaampi, mutta paketti säilyy aina ehjänä.";
        maxSize = new double[] {30, 30, 30};
        maxWeight = 30;
        maxDeliveryDistance = 150;
        
    }
    
    /**
     * Turvallinen kuljetus, joten tavarat eivät hajoa.
     * @param i
     * @return 
     */
    @Override
    protected boolean breakOnDelivery(Item i) {
        return false;
    }
}

class PostClass_3 extends PackageClass {

    public PostClass_3() {
        
        name = "3";
        information = "Hitain, ja paketti todennäköisesti hajoaa matkalla.";
        maxSize = new double[] {100, 100, 100};
        maxWeight = 100;
        maxDeliveryDistance = 150;
        
    }
    
    @Override
    protected boolean breakOnDelivery(Item i) {
        double tn;
        double[] iSize = i.getSize();
        boolean hasBadDay = Math.random() < 0.8;
        boolean isBroken;
        
        double mVolume = this.maxSize[0] * this.maxSize[1] * this.maxSize[2];
        double iVolume = iSize[0] * iSize[1] * iSize[2];
        
        /**
         * koko ja paino vaikuttavat hajoamisen todennäköisyyteen
         */
        if (hasBadDay) {
            tn = (iVolume/mVolume) * (i.getWeight()/maxWeight) * (0.2 * Math.random() + 0.9);
            isBroken = tn < 0.8;
        } else {
            isBroken = Math.random() < 0.5;
        }
        
        return i.getCanBreak() && isBroken;
    }

}