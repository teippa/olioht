/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package timotei;



import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author n1602
 */
public final class Storage {
    
    private static Storage storage = null;
    
    DataIE dataIE;
    
    ArrayList<PackageClass> packages;
    ArrayList<PackageClass> packagesOnTheWay;
    ArrayList<Item> items;
    
    
    private Storage() {
        //createPostClasses();
        packages = new ArrayList();
        items = new ArrayList();
        packagesOnTheWay = new ArrayList();
        
        dataIE = new DataIE(this);
        dataIE.importItems();
        dataIE.importPackages();
    }
    
    public static Storage getInstance() {
        if (storage == null) {
            storage = new Storage();
        }
        return storage;
    }
    
    /**
     * Creates a new item and adds it to ArrayList if successful
     * Returns null, if adding failed
     * @param name
     * @param size
     * @param weight
     * @param canBreak
     * @return 
     */
    public Item addNewItem(String name, String size, String weight, boolean canBreak) {
        Item i = new Item(name, size, weight, canBreak);
        
        if (!items.contains(i) && i.isCreationSuccess()) {
            items.add(i);
        } else {
            i = null;
        }
        
        return i;
    }
    
    /**
     * Creates a new item and tries to add it to a package.
     * Returns null, if adding failed
     * @param name
     * @param size
     * @param weight
     * @param canBreak
     * @param packageClass
     * @return 
     */
    public PackageClass addNewPackage(String name, String size, String weight, boolean canBreak, String packageClass) {
        Item i = new Item(name, size, weight, canBreak);
        
        return addNewPackage(i, packageClass);
    }
    /**
     * Tries to add item to package
     * Returns null, if adding failed
     * @param i
     * @param packageClass
     * @return 
     */
    public PackageClass addNewPackage(Item i, String packageClass) {
        boolean addSuccess;
        
        PackageClass pc = null;
        
        switch (packageClass) {
            case "1":
                pc = new PostClass_1();
                addSuccess = pc.addItem(i);
                break;
            case "2":
                pc = new PostClass_2();
                addSuccess = pc.addItem(i);
                break;
            case "3":
                pc = new PostClass_3();
                addSuccess = pc.addItem(i);
                break;
            default:
                addSuccess = false;
                System.out.println("Paketin lisääminen epäonnistui:\n\t");
        }
        
        if (addSuccess && pc != null) {
            packages.add(pc);
        } else {
            pc = null;
        }
        
        return pc;
    }
    
    public boolean removePackage(PackageClass pc) {
        return packages.remove(pc);
    }
    
    public boolean removeItem(Item i) {
        return items.remove(i);
    }
    
    public ArrayList getPackagesOnTheWay() {
        return packagesOnTheWay;
    }
    
    public void addPackageOnTheWay(PackageClass pc) {
        packagesOnTheWay.add(pc);
    }
    
    public boolean removePackageOnTheWay(PackageClass pc) {
        return packagesOnTheWay.remove(pc);
    }
    
    public ArrayList getPackages() {
        return packages;
    }
    
    public ArrayList getItems() {
        return items;
    }

    void exportItems() {
        dataIE.exportItems();
    }

    void exportPackages() {
        dataIE.exportPackages();
    }
    
    /**
     * Updates log
     * @param o
     * @param event 
     */
    public void updateLoki(Object o, String event) {
        if (o == null) {
            return;
        }
        String s;
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy 'klo' HH:mm");
        Date now = new Date();
        
        
        switch (event.toLowerCase()) {
            case "add":
                s = dateFormat.format(now) + "    Lisätty " + o.toString();
                break;
            case "remove":
                s = dateFormat.format(now) + "    Poistettu " + o.toString();
                break;
            case "send":
                s = dateFormat.format(now) + "    Lähetetty " + o.toString();
                break;
            default:
                s = dateFormat.format(now) + "    Corrupted log entry :(";
                
        }
        
        dataIE.appendLoki(s);
    }
    
    public String readLoki() {
        return dataIE.readLoki();
    }
    
}
