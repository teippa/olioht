/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package timotei;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.URL;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 *
 * @author n1602
 */
public class WebParser {
    
    
    /**
     * Parses string with xml syntax, and turns it into a document.
     * @param source
     * @return 
     */
    public Document parseXMLString(String source) {
        
        Document doc = null;
        
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            doc = dBuilder.parse(new InputSource(new StringReader(source)));
            doc.getDocumentElement().normalize();
        } catch (ParserConfigurationException e) {
            System.err.println("Caught ParserConfigurationException!");
            System.out.println("Location data parsing failed_1! (parseXML in WebParser.java)");
        } catch (IOException e) {
            System.err.println("Caught IOException!");
            System.out.println("Location data parsing failed_2! (parseXML in WebParser.java)");
        } catch (SAXException e) {
            System.err.println("Caught SAXException!");
            System.out.println("Location data parsing failed_3! (parseXML in WebParser.java)");
        }
        
        return doc;
    }
    
    /**
     * Parses string with xml syntax, and turns it into a document.
     * @param fileName
     * @return 
     */
    public Document parseXMLFile(String fileName) {
        
        Document doc = null;
        
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            doc = dBuilder.parse(fileName);
            doc.getDocumentElement().normalize();
        } catch (ParserConfigurationException e) {
            //System.err.println("Caught ParserConfigurationException!");
            System.out.println("Location data parsing failed_1! (parseXML in WebParser.java)");
            doc = null;
        } catch (IOException e) {
            //System.err.println("Caught IOException!");
            System.out.println("Location data parsing failed_2! (parseXML in WebParser.java)");
            doc = null;
        } catch (SAXException e) {
            //System.err.println("Caught SAXException!");
            System.out.println("Location data parsing failed_3! (parseXML in WebParser.java)");
            doc = null;
        }
        
        return doc;
    }
    
    /**
     * reads source code from URL and returns it as a string
     * @param urlStr
     * @return gatheredData
     */
    public String readSource(String urlStr) {
        
        String inputLine, gatheredData = "";
        
        try {
            URL url = new URL(urlStr);
        
            BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
            
            while((inputLine = in.readLine()) != null) {
                gatheredData += inputLine+"\n";
            }
        } catch (IOException ex) {
            System.out.println("Location data parsing failed! (readSource in WebParser.java)");
            gatheredData = null;
        }
        
        return gatheredData;
    }
    
    /**
     * Returns an element from document based on input tag
     * @param tag
     * @param e
     * @return 
     */
    public String getValue(String tag, Element e) {
        return e.getElementsByTagName(tag).item(0).getTextContent();
    }
    
}
